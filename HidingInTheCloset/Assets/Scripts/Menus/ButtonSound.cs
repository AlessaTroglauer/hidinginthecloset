﻿using UnityEngine;
using System.Collections;

public class ButtonSound : MonoBehaviour
{
    //Method to call when hovering over button
    public void PlayHoverSound()
    {
        //Play sound
        FindObjectOfType<AudioManager>().Play("ButtonHover");
    }

    //Method to call when clicking button
    public void PlayClickSound()
    {
        //Play sound
        FindObjectOfType<AudioManager>().Play("ButtonClick");
    }
}
