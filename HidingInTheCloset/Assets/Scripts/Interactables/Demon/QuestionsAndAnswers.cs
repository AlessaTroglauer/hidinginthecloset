﻿
[System.Serializable]
public class QuestionsAndAnswers 
{
    //Question
    public string question;

    //Array of answers
    public string[] answer;

    //Number of correct answer
    public int correctAnswer;
}
