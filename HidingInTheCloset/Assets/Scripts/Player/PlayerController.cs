﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    //Movement variables 
    [SerializeField]
    private float moveSpeed = default;
    private float moveInput = default;
    private bool facingRight = true; 

    //Jump and fall variables 
    [SerializeField]
    private float jumpForce = default;
    private Rigidbody2D rb2d = default;
    private bool canDoubleJump = default;
    [SerializeField]
    private float fallMultiplier = default;

    //Groundcheck Variables
    [SerializeField]
    private LayerMask groundLayerMask = default;
    private BoxCollider2D boxCollider2d = default;

    //References for animations and effects
    private Animator playerAnimator = default;
    [SerializeField]
    private ParticleSystem runParticles = default;
    [SerializeField]
    private ParticleSystem jumpParticles = default;
    [SerializeField]
    private ParticleSystem landParticles = default;
    private bool wasOnGround = default; 

    private void Start()
    {
        //Setting references 
        rb2d = GetComponent<Rigidbody2D>();
        boxCollider2d = GetComponent<BoxCollider2D>();
        playerAnimator = GetComponent<Animator>();
    }
    private void Update()
    {
        //Set moveInput to Horizontal Axis Input
        moveInput= Input.GetAxis("Horizontal");
        
        //Flip player
        if (facingRight == false && moveInput > 0)
        {
            Flip();
        }
        else if (facingRight == true && moveInput < 0)
        {
            Flip();
        }

        //Enable double jump when grounded
        if (IsGrounded())
        {
            canDoubleJump = true;
        }

        //Check if jump button is pressed
        if (Input.GetButtonDown("Jump"))
        {
            //Call Jump if player is grounded
            if (IsGrounded())
            {
                Jump(); 
            }
            else
            {
                //Let player jump again if still able to double jump 
                if (canDoubleJump)
                {
                    Jump();

                    //Disable doublejump
                    canDoubleJump = false;
                }
            }
        }

        //Different jump height depending on button press
        if (Input.GetButtonUp("Jump") && rb2d.velocity.y > 0)
        {
            rb2d.velocity = rb2d.velocity * 0.5f;
        }

        //Dragging player to ground when falling depending on fallmultiplier 
        if (rb2d.velocity.y < 0)
        {
            rb2d.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }

        //Call set particles and animations functions
        SetParticles(); 
        SetAnimationState();
    }

    //Move Player
    private void FixedUpdate()
    {
        Walk();
    }

    //Change velocity of rigidbody depending on speed and move input
    private void Walk()
    {
        rb2d.velocity = new Vector2(moveInput * moveSpeed, rb2d.velocity.y);
    }

    //Flip function 
    private void Flip()
    {
        facingRight = !facingRight;
        Vector3 scaler = transform.localScale;
        scaler.x *= -1;
        transform.localScale = scaler;
    }

    //Jump function
    private void Jump()
    {
        //Set velocity of rigidbody
        rb2d.velocity = Vector2.up * jumpForce;
        jumpParticles.Play();
    }

    //Groundcheck
    private bool IsGrounded()
    {
        //Shoot raycast from center downwards 
        //check for ground layer 
        RaycastHit2D raycastHit = Physics2D.BoxCast(boxCollider2d.bounds.center, boxCollider2d.bounds.size - new Vector3(0.01f, 0f, 0f), 0, Vector2.down, 0.1f, groundLayerMask);

        return raycastHit.collider != null;
    }

    //Set animation states 
    private void SetAnimationState()
    {
        //Set speed depending on moveInput
        playerAnimator.SetFloat("Speed", Mathf.Abs(moveInput));

        //Set IsFalling and IsJumping when player is grounded
        if (rb2d.velocity.y == 0)
        {
            playerAnimator.SetBool("IsFalling", false);
            playerAnimator.SetBool("IsJumping", false);
        }

        //Set IsJumping to true when rigidbody is greater than null
        if(rb2d.velocity.y > 0)
        {
            playerAnimator.SetBool("IsJumping", true);       
        }

        //Set IsFalling to true when rigidbody is less than null and IsJumping to false
        if (rb2d.velocity.y < 0)
        {
            playerAnimator.SetBool("IsFalling", true);
            playerAnimator.SetBool("IsJumping", false);
        }

    }

    private void SetParticles()
    {
        //Set RunParticles if horizontal Input is greater than 0 
        if ((Input.GetAxis("Horizontal") != 0) && IsGrounded())
        {
            runParticles.Play();
        }
        else
        {
            runParticles.Stop();
        }

        //Set LandParticles when player reaches ground 
        if(IsGrounded())
        {
            if(wasOnGround == true)
            {
                landParticles.Play();
                wasOnGround = false; 
            }
        }
        else
        {
            wasOnGround = true; 
        }
        
    }
}
