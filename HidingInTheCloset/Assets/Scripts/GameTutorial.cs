﻿using UnityEngine;
using System.Collections;
using TMPro;

public class GameTutorial : MonoBehaviour
{
    //Reference for the text to display 
    [SerializeField]
    private TextMeshProUGUI tutorialText = default;

    private void Start()
    {
        //Disable text at beginning
        tutorialText.enabled = false;
    }

    //Show text when entering trigger
    private void OnTriggerEnter2D(Collider2D collision)
    {
        tutorialText.enabled = true; 
    }

    //Hide text when exiting trigger
    private void OnTriggerExit2D(Collider2D collision)
    {
        tutorialText.enabled = false;
    }
}
