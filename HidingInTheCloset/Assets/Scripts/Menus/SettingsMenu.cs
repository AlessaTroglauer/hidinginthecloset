﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine.Audio; 
using UnityEngine;
using UnityEngine.UI;
using TMPro; 

public class SettingsMenu : MonoBehaviour
{
    //References
    [SerializeField]
    private Slider masterVolumeSlider = default;

    [SerializeField]
    private Slider musicVolumeSlider = default;

    [SerializeField]
    private Slider sfxVolumeSlider = default;

    [SerializeField]
    private AudioMixer audioMixer = default;

    [SerializeField]
    private TMP_Dropdown resolutionDropdown = default; 

    //Array of resolutions
    private Resolution[] resolutions = default;

    private void Awake()
    {
        //Check if value has been set
        //Access saved slider values
        //Set slider value to saved value
        if (PlayerPrefs.HasKey("MasterVolume"))
        {
            SetMasterVolume(PlayerPrefs.GetFloat("MasterVolume"));
            masterVolumeSlider.value = PlayerPrefs.GetFloat("MasterVolume");
        }

        if (PlayerPrefs.HasKey("MusicVolume"))
        {
            SetMusicVolume(PlayerPrefs.GetFloat("MusicVolume"));
            musicVolumeSlider.value = PlayerPrefs.GetFloat("MusicVolume");
        }

        if (PlayerPrefs.HasKey("SfxVolume"))
        {
            SetSfxVolme(PlayerPrefs.GetFloat("SfxVolume"));
            sfxVolumeSlider.value = PlayerPrefs.GetFloat("SfxVolume");
        }

    }

    private void Start()
    {
        //Get availabe resolutions
        resolutions = Screen.resolutions;

        //Clear default options
        resolutionDropdown.ClearOptions();

        //Turn array of resolutions into list of strings
        List<string> options = new List<string>();

        //Set default resolution index
        int currentResolutionIndex = 0;

        //Loop through each element of resolution array
        for (int i = 0; i < resolutions.Length; i++)
        {
            //Create string for each element
            string option = resolutions[i].width + "x" + resolutions[i].height;

            //Add element to options list
            options.Add(option);

            //Compare width and height of resolutions
            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height )
            {
                //Storing index of correct resolution
                currentResolutionIndex = i; 
            }
        }

        //Add options to dropdown
        resolutionDropdown.AddOptions(options);

        //Display current resolution
        resolutionDropdown.value = currentResolutionIndex;
        resolutionDropdown.RefreshShownValue();

    }

    //Set Resolution
    public void SetResolution(int resolutionIndex)
    {
        Resolution resolution = resolutions[resolutionIndex];
        Screen.SetResolution(resolution.width, resolution.height, Screen.fullScreen);
    }

    //Gets called when moving slider
    //Sets value of slider to Master Volume
    //Save slider value
    public void SetMasterVolume(float volume)
    {
        audioMixer.SetFloat("MasterVolume", Mathf.Log10(volume) * 20);
        PlayerPrefs.SetFloat("MasterVolume", volume);
    }

    //Gets called when moving slider
    //Sets value of slider to Music Volume
    //Save slider value
    public void SetMusicVolume(float volume)
    {
        audioMixer.SetFloat("MusicVolume", Mathf.Log10(volume) * 20);
        PlayerPrefs.SetFloat("MusicVolume", volume);
    }

    //Gets called when moving slider
    //Sets value of slider to Sfx Volume
    //Save slider value
    public void SetSfxVolme(float volume)
    {
        audioMixer.SetFloat("SfxVolume", Mathf.Log10(volume) * 20);
        PlayerPrefs.SetFloat("SfxVolume", volume);
    }

    //Set Quality settings
    public void SetQuality (int qualityIndex)
    {
        QualitySettings.SetQualityLevel(qualityIndex);
    }

    //Set Fullscreen
    public void SetFullscreen(bool isFullscreen)
    {
        Screen.fullScreen = isFullscreen; 
    }
}


