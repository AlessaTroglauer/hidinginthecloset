﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnswerScript : MonoBehaviour
{
    public bool isCorrect = false;
    public QuizManager quizManager;

    //Method to call when clicked on button
    public void Answer()
    {
        //Checking if the button pressed is the correct one and call corresponding method
        if (isCorrect == true)
        {
            Debug.Log("Correct");
            quizManager.AnsweredCorrect(); 

        }
        else
        {
            Debug.Log("Wrong Answer");

            quizManager.AnsweredWrong();
        }
    }
}
