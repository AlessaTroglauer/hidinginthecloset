﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    //reference to what to follow
    [SerializeField]
    public Transform target = default;

    //Smooth amount 
    [SerializeField]
    private float smoothSpeed = default;

    //Offset
    [SerializeField]
    private Vector3 offset = default;

    private void Start()
    {
        //Locking camera to target + offset at beginning
        transform.position = target.position + offset;     
    }
 
    private void FixedUpdate()
    {
        //Set desired position position of target + offset
        Vector3 desiredPosition = target.position + offset;
        
        //Smooth out position
        Vector3 smoothedPosition = Vector3.Lerp(transform.position, desiredPosition, smoothSpeed * Time.deltaTime); 

        //Set camera position to smoothed position
        transform.position = smoothedPosition;
    }
}
