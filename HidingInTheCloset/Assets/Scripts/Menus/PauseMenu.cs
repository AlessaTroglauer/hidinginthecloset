﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    //sets Game to inactive
    public static bool gameIsPaused = false;

    //Reference to pauseMenuUI
    [SerializeField]
    private GameObject pauseMenuUI = default;
    
    void Update()
    {
        //Check for Esc  press
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            //Continues game if paused or pauses it if not paused 
            if (gameIsPaused)
            {
                Resume();

            }
            else
            {
                Pause();
            }
        }
    }

    //Continue game and deactivate pause menu UI
    public void Resume()
    {
        pauseMenuUI.SetActive(false);
        Time.timeScale = 1f;
        gameIsPaused = false;
    }

    //Pause the game and activate the UI of pause Menu
    void Pause()
    {
        FindObjectOfType<AudioManager>().Play("OpenPauseMenu");
        pauseMenuUI.SetActive(true);
        Time.timeScale = 0f;
        gameIsPaused = true;
    }

    //Load Start Menu
    public void LoadMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("StartMenu");
    }

    //Quit Application
    public void QuitGame()
    {
        Application.Quit();
    }
}
