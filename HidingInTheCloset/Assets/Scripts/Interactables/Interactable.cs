﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events; 

public class Interactable : MonoBehaviour
{
    //Variable setting if is in range
    private bool isInRange = false;

    //Variable to when player has interacted
    private bool hasInteracted = false;

    //key needed to interact
    [SerializeField]
    private KeyCode interactKey = default;

    //Reference to E button to display
    [SerializeField]
    private GameObject eButtonSprite = default;

    //Actions to call
    [SerializeField]
    private UnityEvent interactAction = default;
    [SerializeField]
    private UnityEvent closeInteractAction = default;

    void Update()
    {
        //Check if Interact Key is pressed
        if (Input.GetKeyDown(interactKey))
        {
            //If in range and has not interacted yet
            if (isInRange && hasInteracted == false)
            {
                //Execute interact action
                interactAction.Invoke();

                //Set hasInteracted to true
                hasInteracted = true;


            }
            else if(hasInteracted == true)
            {
                //If hasInteracted Call close interact action and make object interactable again
                closeInteractAction.Invoke();
                hasInteracted = false;
            }
        }
    }

    //Set is in Range to true when player enters collider and activate e button sprite
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            isInRange = true;
            eButtonSprite.SetActive(true);   
        }
    }

    //Set is in Range to false when player leaves collider and deactivate e button sprite
    private void OnTriggerExit2D(Collider2D collision)
    {
        isInRange = false;
        eButtonSprite.SetActive(false);
    }
}
