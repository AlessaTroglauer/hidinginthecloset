﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class Sound 
{
    //Name of audio 
    public string name; 

    //Reference to audio clip
    public AudioClip clip;

    //Volume
    [Range(0f, 1f)]
    public float volume;

    //Pitch
    [Range(.1f, 3f)]
    public float pitch;

    //Loop
    public bool loop;

    //Reference to audio mixer group
    public AudioMixerGroup outputAudioMixerGroup; 

    //Audio source
    [HideInInspector]
    public AudioSource source;
}
