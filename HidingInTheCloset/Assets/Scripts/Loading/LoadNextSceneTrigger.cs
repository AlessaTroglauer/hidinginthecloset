﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement; 

public class LoadNextSceneTrigger : MonoBehaviour
{
    //Name of scene which will load next
    [SerializeField]
    private string sceneNameToLoad = default;

    //Time to transition
    [SerializeField]
    private float transitionTime = default; 

    //Reference to animator of crossfade
    [SerializeField]
    private Animator crossfade = default;

    //Load scene if player enters trigger
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            LoadNextScene(); 
        }
    }

    //Function to call LoadScene
    public void LoadNextScene()
    {
        StartCoroutine(LoadScene());
    }

    //Coroutine to delay the code / loading of scene
    IEnumerator LoadScene()
    {
        //Trigger Crossfade
        crossfade.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene(sceneNameToLoad);
    }
}
