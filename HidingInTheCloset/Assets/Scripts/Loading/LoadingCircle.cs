﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LoadingCircle : MonoBehaviour
{
    //References to Loading circle components
    [SerializeField]
    private RectTransform fxHolder = default;

    [SerializeField]
    private Image circleImage = default;

    [SerializeField]
    private TextMeshProUGUI textProgress = default;

    //Variable to measure progress
    private float progress = default;

    private void Update()
    {
        //Check if is smaller or equal to 1 
        if(progress <= 1)
        {
            //Increase progress over time and devideit by 10
            progress += Time.deltaTime / 10;

            //Sett fill amount to progress
            circleImage.fillAmount = progress;

            //Round up progress text and set it to string
            textProgress.text = Mathf.Floor(progress * 100).ToString();

            //Move particles depending on progress
            fxHolder.rotation = Quaternion.Euler(new Vector3(0f, 0f, -progress * 360));
        }
    }
}
