﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMenuFairyLight: MonoBehaviour
{
    //Reference to light Animator
    private Animator lightAnimator = default;

    private void Start()
    {
        //Setting reference
        lightAnimator = GetComponent<Animator>();
    }
    
    //Call animation when hovering over object collider
    private void OnMouseOver()
    {
        lightAnimator.SetBool("Highlighted", true);
    }
}
