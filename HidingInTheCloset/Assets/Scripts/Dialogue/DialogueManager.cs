﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro; 
using UnityEngine.UI; 

public class DialogueManager : MonoBehaviour
{
    //Refernce to Text Object
    public TextMeshProUGUI dialogueText;

    //Variable keeping track all of all the sentences in the current Dialogue
    private Queue<string> sentences;

    public Animator animator; 

    void Start()
    {
        //Initializing 
        sentences = new Queue<string>();
    }

    //Start Dialogoue 
    public void StartDialogue(Dialogue dialogue)
    {
        animator.SetBool("IsOpen", true);
        sentences.Clear(); 
    
        //Looping through Sentences in Dialogue and adding them to Queue
        foreach (string sentence in dialogue.sentences)
        {
            sentences.Enqueue(sentence); 
        }

        DisplayNextSenctence();
    }

    public void DisplayNextSenctence()
    {
        //Checking if there are more Sentences in the Queue left 
        if (sentences.Count == 0)
        {
            EndDialogue();
            return; 
        }

        //Getting next Sentence in the Queue
        string sentence = sentences.Dequeue();
        StopAllCoroutines();
        StartCoroutine(TypeSentence(sentence));
    }

    IEnumerator TypeSentence (string sentence)
    {
        dialogueText.text = "";
        foreach (char letter in sentence.ToCharArray())
        {
            dialogueText.text += letter;
            yield return null;
        }
    }

    private void EndDialogue()
    {
        animator.SetBool("IsOpen", false);
    }
}
