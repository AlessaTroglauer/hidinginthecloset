﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuCameraController : MonoBehaviour
{
    //Setting first and current mount
    [SerializeField]
    private Transform currentMount = default;

    //Setting speed of camera movement 
    [SerializeField]
    private float speedFactor = default;

    void Update()
    {
        //Move camera to current mount
        transform.position = Vector3.Lerp(transform.position, currentMount.position, speedFactor * Time.deltaTime);
        transform.rotation = Quaternion.Slerp(transform.rotation, currentMount.rotation, speedFactor * Time.deltaTime);
    }

    //Function to call from buttons
    public void SetMount(Transform newMount)
    {
        //Set current mount to new mount
        currentMount = newMount; 
    }
}
