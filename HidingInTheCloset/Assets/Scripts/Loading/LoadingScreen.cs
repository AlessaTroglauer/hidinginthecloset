﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class LoadingScreen : MonoBehaviour
{
    //Name of scene which will load next
    [SerializeField]
    private string sceneNameToLoad = default;

    //Time until the next scene loads
    [SerializeField]
    private float delaybeforeLoading = default;

    //Variable for the time passed
    private float timeElapsed = default;

    //Time to transition
    [SerializeField]
    private float transitionTime = default;

    //Reference to animator of crossfade
    [SerializeField]
    private Animator crossfade = default;

    void Update()
    {
        //Increase time elapsed
        timeElapsed += Time.deltaTime;

        //Load scene if enough time elapsed 
        if (timeElapsed > delaybeforeLoading)
        {
            LoadNextScene();
        }
    }

    //Function to call LoadScene
    public void LoadNextScene()
    {
        StartCoroutine(LoadScene());
    }

    //Coroutine to delay the code / loading of scene
    IEnumerator LoadScene()
    {
        //Trigger Crossfade
        crossfade.SetTrigger("Start");
        yield return new WaitForSeconds(transitionTime);
        SceneManager.LoadScene(sceneNameToLoad);
    }
}

