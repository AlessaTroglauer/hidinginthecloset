﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogueTrigger : MonoBehaviour
{
    public bool hasInteracted;

    //Reference to Dialogue Script
    public Dialogue dialogue; 

    public void TriggerDialogue()
    {
        //Only interactable once
        if (!hasInteracted)
        {
            //Locating DiaglogueManager and call Start Function
            FindObjectOfType<DialogueManager>().StartDialogue(dialogue);    
        }
    } 
}
