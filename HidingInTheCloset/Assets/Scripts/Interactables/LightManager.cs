﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightManager : MonoBehaviour
{
    //Variable for number of lights to dispalay
    public GameObject[] inGameMenuLights;

    //Set countof collected lights to null by default
    public int collectedLightsCount = 0; 

    //Reset Lights  
    public void ResetStars()
    {
        collectedLightsCount = 0; 

        //Deactivate lights in UI
        for(int i = 0; i < 3; i++)
        {
            inGameMenuLights[i].SetActive(false);
        }
    }
    
    //Function to call when collecting light
    public void StarCollected()
    {
        //Enable light in UI
        inGameMenuLights[collectedLightsCount].SetActive(true);

        //Increase collectedd light count
        collectedLightsCount++;
    }
}
