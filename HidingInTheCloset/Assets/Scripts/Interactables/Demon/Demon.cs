﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Demon : MonoBehaviour
{
    //Lights needed to start Quiz
    [SerializeField]
    private int lightsToInteract = default;

    [SerializeField]
    private GameObject interactableScriptToBlock = default;

    //Sentence to show when player collected enough lights
    [TextArea(1, 10)]
    public string dialogueEnough;

    //Sentence to show when player did not collect enough lights
    [TextArea(1, 10)]
    public string dialogueNotEnough;

    //References 
    [SerializeField]
    private TextMeshProUGUI dialoguetext = default;
    [SerializeField]
    private Animator dialogueBoxAnimator = default;

    [SerializeField]
    private GameObject quizPanel = default;
    [SerializeField]
    private Animator quizAnimator = default;

    //Function to call when player has not interacted yet
    public void StartQuiz(GameObject obj)
    {
        //Freeze time
        Time.timeScale = 0;

        //Reference to light manager 
        LightManager lightManager = obj.GetComponent<LightManager>();

        //Play Sound
        FindObjectOfType<AudioManager>().Play("DemonEncounter");

        //Checking amount of lights
        if (lightManager.collectedLightsCount == lightsToInteract)
        {
            //Enable Dialogue
            dialoguetext.text = dialogueEnough;
            dialogueBoxAnimator.SetBool("IsOpen", true); 
            dialoguetext.enabled = true; 
        }
        else if((lightManager.collectedLightsCount < lightsToInteract))
        {
            //Enable Dialogue
            dialogueBoxAnimator.SetBool("IsOpen", true);
            dialoguetext.enabled = true;
            dialoguetext.text = dialogueNotEnough;
        }
    }

    //Function to call when StartQuiz was called before and player interacts again
    public void ContinueAction(GameObject obj)
    {
        //Reference to light manager 
        LightManager lightManager = obj.GetComponent<LightManager>();

        //Checking amount of lights
        if (lightManager.collectedLightsCount == lightsToInteract)
        {
            //Disable dialogue
            dialoguetext.enabled = false;
            dialogueBoxAnimator.SetBool("IsOpen", false);

            //Activate Quiz
            quizPanel.SetActive(true);
            quizAnimator.SetBool("IsOpen", true);
            Debug.Log("Starting Quiz");

            //Block interactable script
            interactableScriptToBlock.SetActive(false);


        }
        else if ((lightManager.collectedLightsCount < lightsToInteract))
        {
            //Disable dialogue
            dialogueBoxAnimator.SetBool("IsOpen", false);
            dialoguetext.enabled = false;

            //Set time back to normal
            Time.timeScale = 1; 
        }
    }
}




