﻿using UnityEngine;
using UnityEngine.Audio;
using System; 

public class AudioManager : MonoBehaviour
{
    //List of sounds
    public Sound[] sounds; 

    void Awake()
    {
        //Loop through list
        foreach (Sound s in sounds)
        {
            //Add audio source and set variables
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volume;          
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
            s.source.outputAudioMixerGroup = s.outputAudioMixerGroup;
        }
    }

    //Play Theme audio when starting level
    private void Start()
    {
        Play("Theme");
    }

    //Public method to call from outside the class 
    public void Play(string name)
    {
        //Loop through all sounds and find soud with the appropriate name
        Sound s = Array.Find(sounds, sound => sound.name == name);

        //Check if sound exists
        if ( s == null)
        {
            Debug.LogWarning("Sound: " + name + " not found!"); 
            return; 
        }

        //Play sound
        s.source.Play(); 
    }
}
