﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Dialogue 
{
    //Sentences loaded into Queue
    [TextArea(1, 10)]
    public string[] sentences; 
}
