﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class SliderScript : MonoBehaviour
{
    //References 
    [SerializeField]
    private Slider slider = default;
    [SerializeField]
    private TextMeshProUGUI percentageText = default;
    [SerializeField]
    private ParticleSystem particleSys = default;

    //Variable for slider value
    [SerializeField]
    private float value = default; 
    
    void Start()
    {
        //Set reference 
        particleSys = GetComponentInChildren<ParticleSystem>();

        //Set value
        value = slider.value;

        //Call textUpdate and feed value
        TextUpdate(slider.value);
    }

    //Update percentage text based on slider value 
    public void TextUpdate(float value)
    {
        percentageText.text = Mathf.RoundToInt(value * 100) + "%";
    }

    //Play Particle System
    public void ActivateParticleSystem()
    {
        particleSys.Play();
    }

    //Stop Particle System
    public void StopParticleSystem()
    {
        particleSys.Stop();
    }
}
