﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class Diary : MonoBehaviour
{
    //Reference to components of diary pages
    [SerializeField]
    private GameObject diaryPage = default;

    [SerializeField]
    private TextMeshProUGUI diaryPageText = default;

    [SerializeField]
    private GameObject drawing = default;

    //Text of diary 
    [TextArea(1, 10)]
    public string diaryPageContent = default;

    // Start is called before the first frame update
    void Start()
    {
        //Deactivate diary page at beginning
        diaryPage.SetActive(false);
    }

    //Function to call when interacting first time 
    public void DisplayDiary()
    {
        //Play Sound
        FindObjectOfType<AudioManager>().Play("OpenDiary");

        //Activate diary page with, load text set text and show specific drawing
        diaryPage.SetActive(true);
        diaryPageText.text = diaryPageContent;
        drawing.SetActive(true);

        //Freeze time
        Time.timeScale = 0f;

        Debug.Log("Opening");
    }

    //Function to call when player has already interacted
    public void CloseDiary()
    {     
        //Deactivate diary page
        drawing.SetActive(false);
        diaryPage.SetActive(false);

        //Set time back to normal
        Time.timeScale = 1f;

        Debug.Log("Closing");

    }
}
