﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using TMPro;

public class QuizManager : MonoBehaviour
{
    //List of questions and asnwers
    public List<QuestionsAndAnswers> QnA;

    //Reference to button options
    public GameObject[] options;

    //Index of currentquestion
    public int currentQuestion;

    //Number of total questions
    private int totalQuestions = 0;
    //score
    public int score;

    public bool quizCompleted = false;

    //References
    [SerializeField]
    private GameObject fairyLight = default;
    [SerializeField]
    private GameObject wallToUnlock = default;
    [SerializeField]
    private GameObject enemyToDefeat = default;
    [SerializeField]
    private GameObject quizPanel = default;
    [SerializeField]
    private TextMeshProUGUI questionText = default;
    [SerializeField]
    private Animator animator = default;
    [SerializeField]
    private Animator demonAnimator = default;
    [SerializeField]
    private Animator demonWallAnimator = default;
    [SerializeField]
    private GameObject interactableScriptToActivate = default;

    public List<QuestionsAndAnswers> copyOfList = new List<QuestionsAndAnswers>();
     
    private void Start()
    {
        copyOfList.AddRange(QnA);
        //quizPanel.SetActive(false);
        totalQuestions = QnA.Count; 
        GenerateQuestion();
    }

    private void GenerateQuestion()
    {
        //if questions left
        if(QnA.Count > 0)
        {
            //Get random questions
            currentQuestion = Random.Range(0, QnA.Count);

            //Set text
            questionText.text = QnA[currentQuestion].question;

            //Set Answer
            SetAnswer();
        }

        //If no questions left
        if (QnA.Count <= 0 && score == totalQuestions )
        {
            Debug.Log("Completed");
            //Play Audio
            FindObjectOfType<AudioManager>().Play("QuizFinish");
            
            //Set time back to normal
            Time.timeScale = 1;
           
            //Call animations
            demonAnimator.SetTrigger("Vanish"); 
            fairyLight.SetActive(true);
            //animator.SetBool("IsOpen", false);
            demonWallAnimator.SetTrigger("Vanish");

            //Deactivate / destroy objects
            Destroy(enemyToDefeat, 2);
            Destroy(wallToUnlock, 1);
            quizPanel.SetActive(false);
        }
        else if(QnA.Count <= 0 && score != totalQuestions)
        {
            Debug.Log("Failed");
            //Play Audio
            FindObjectOfType<AudioManager>().Play("QuizFail");

            //animator.SetBool("IsOpen", false);

            //Set time back to normal
            Time.timeScale = 1;

            //Reactivate demon
            interactableScriptToActivate.SetActive(true);

            //Deactivate quiz panel
            quizPanel.SetActive(false);

            //Reset Quiz
            score = 0;
            ResetList();
            copyOfList = new List<QuestionsAndAnswers>();
            Start();
        }
    }

    private void SetAnswer()
    {
        //Go through all buttons
        for (int i = 0; i < options.Length; i++)
        {
            //Set all answers to false
            options[i].GetComponent<AnswerScript>().isCorrect = false;

            //Assign answer to buttons
            options[i].transform.GetChild(3).GetComponent<TextMeshProUGUI>().text = QnA[currentQuestion].answer[i];

            //If answer is correct set is correct to true
            if (QnA[currentQuestion].correctAnswer == i + 1)
            {
                options[i].GetComponent<AnswerScript>().isCorrect = true;
            }
        }
    }

    //Method to call when aswered correctly
    public void AnsweredCorrect()
    {
        //Increase score
        score++;

        //Remove current question
        QnA.RemoveAt(currentQuestion);

        //Generate next question
        GenerateQuestion();   
    }

    //Method to call when aswered wrong
    public void AnsweredWrong()
    {
        //Remove current question
        QnA.RemoveAt(currentQuestion);

        //Generate next quesion
        GenerateQuestion(); 
    }

    //Reset List
    public void ResetList()
    {
        //Remove alls questions
        QnA.Clear();
        //Add all questions
        QnA.AddRange(copyOfList);
    }
}
