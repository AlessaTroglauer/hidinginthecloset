﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectingLight : MonoBehaviour
{
    //References
    [SerializeField]
    private GameObject particles = default;

    [SerializeField]
    private Animator playerPointLightAnimator = default; 

    private void OnTriggerEnter2D(Collider2D collision)
    {
        //Check if player entered collider of light
        if (collision.gameObject.CompareTag("Player"))
        {
            //Play sound, animation and activate particles
            FindObjectOfType<AudioManager>().Play("Collect");
            playerPointLightAnimator.SetTrigger("Collected");
            particles.SetActive(true);

            //Destroy light
            Destroy(gameObject);   
            
            //Call Star collected function from LightManager
            FindObjectOfType<LightManager>().StarCollected();
        }
    }
}
